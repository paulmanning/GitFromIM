﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TMASimulator.Models;

namespace TMASimulator.Tests
{
    [TestClass]
    public class PositionTests
    {
        [TestMethod]
        public void Position_ShouldcanCalculateANewPosition_GivenAMovementAndTime()
        {
            const double course = 0; //degrees
            const double speed = 10; //knots
            const double depth = 0; //meters
            double expectedLat = 1.16655409999078;
            double expectedRoundedLat = Math.Round(expectedLat, 5); //1.1665
            const double expectedLon = 1;

            var p = new Position(1, 1);
            var m = new Movement(course, speed, depth); //10 knots
            var t = new TimeSpan(1, 0, 0); // 1 houlr
            Position p2 = p.GetNewPosition(t, m);
            double resultLat = Math.Round(p2.Latitude, 5);
            double resultLon = p2.Longitude;
            Assert.AreEqual(expectedRoundedLat, resultLat);
            Assert.AreEqual(expectedLon, resultLon);
        }

        [TestMethod]
        public void Position_ShouldGetBearingToSecondPostiion()
        {
            const double expectedEastBearing = 90; //east
            const double expectedNorthBearing = 0; //north
            var p1 = new Position(1, 1);
            var p2 = new Position(2, 1); //north
            var p3 = new Position(1, 2); //east
            Assert.AreEqual(expectedNorthBearing, p1.BearingTo(p2));
            Assert.AreEqual(expectedEastBearing, p1.BearingTo(p3));
        }

        [TestMethod]
        public void Position_ShouldBeAbleToTestForEqualityAgainstanotherPosition()
        {
            const double latitude = 10;
            const double longitude = 30;
            var p1 = new Position(latitude, longitude);
            var p2 = new Position(latitude, longitude);

            Assert.IsTrue(p1.Equals(p2));
        }

        [TestMethod]
        public void Position_ShouldRenderLatLonVia_ToString()
        {
            var p1 = new Position(1, 1);
            Assert.AreEqual("1, 1", p1.ToString());
            p1.Latitude = 85.5;
            p1.Longitude = 85.5;
            Assert.AreEqual("85.5, 85.5", p1.ToString());
        }

        [TestMethod]
        public void Positions_ShouldHaveValidLatAndLon_NoWrapping()
        {
            const double latitude = 10;
            const double longitude = 30;

            var p = new Position(latitude, longitude, true);
            Assert.IsTrue(p.IsValid());
        }

        [TestMethod]
        [ExpectedException(typeof (ArgumentOutOfRangeException))]
        public void Position_ShouldThrowExceptionIfBadLatLonWithWrappingOff()
        {
            const double badLat = 91;
            const double badLon = 181;

            // ReSharper disable once UnusedVariable
            var badPosition = new Position(badLat, badLon, true);
        }

        [TestMethod]
        public void Position_ShouldWrapLatAndLonWhenWrapping()
        {
            const double latitude = 10;
            const double longitude = 30;
            const double badLat = 91;
            const double badLon = 181;

            var p = new Position(latitude, longitude, false);
            Assert.IsTrue(p.IsValid());

            var badPosition = new Position(badLat, badLon, false);
            Assert.IsTrue(badPosition.IsValid());
        }

        [TestMethod]
        public void Position_ShouldBeAbleToRenderDistanceToAnotherPostion()
        {
            var p1 = new Position(1, 1);
            var p2 = new Position(0, 0);
            const double expectedResult = 157385;

            double result = Math.Round(p1.GetDistanceTo(p2));
            //should be about 157385, no need to be that precise here
            Assert.AreEqual(expectedResult, result);
        }
    }
}
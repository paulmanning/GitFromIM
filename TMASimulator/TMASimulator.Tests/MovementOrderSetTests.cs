﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TMASimulator.Models;
using TMASimulator.Models.Orders;

namespace TMASimulator.Tests
{
    [TestClass]
    public class MovementOrderSetTests
    {

        [TestMethod]
        public void MovementOrderSet_ShouldBeAbleToAddOrder_and_OrderExecutionTimes()
        {
            var ms = new MovementOrderSet("Crazy Ivan");
            var movementLimit = new MovementCharacteristics();
             

            var order = new StaticCourseChangeLeftOrder(
                5, 15d, movementLimit);
            var order2 = new StaticCourseChangeLeftOrder(
                6, 15d, movementLimit); ;
            
            var value = order.TypeOfChange();
            Assert.AreEqual("Course", value.ToString(), "Shoudl be a Course type order");
            
            ms.Add(order);
            Assert.AreEqual(1, ms.Count(), "Should be able to add one order to the FillInsertionMovementOrderSet");
            ms.Add(order2);
            Assert.AreEqual(2, ms.Count(), "Adding a second should work too");
            
        }
        [TestMethod]
        public void MovementOrderSet_ShouldBeAbleToInsertOrders_AtAnyPointInTheList()
        {
            var ms = new MovementOrderSet("Bombadier General");
            var order1 = new StaticCourseChangeLeftOrder(5, 1d, new MovementCharacteristics());
            var order2 = new StaticCourseChangeLeftOrder(3, 1d, new MovementCharacteristics());
            var order3 = new StaticCourseChangeLeftOrder( 4, 1d, new MovementCharacteristics());
            
            ms.Add(order1);
            ms.Add(order2);
            ms.Add(order3);

            Assert.AreEqual(3, ms.Count(), "three items added");
            Assert.AreEqual(3, ms.GetList().First().StartTimeInSeconds, "First should be time 3");
            Assert.AreEqual(5, ms.GetList().Last().StartTimeInSeconds, "Last should be time 5");
        }
        [TestMethod]
        public void MovementOrderSet_ShouldBeAbleToMarkAnOrder_AsExecuted()
        {
            var ts = FillMovementOrderSetWithOrders();
            ts.MarkComplete(1);
            ts.MarkComplete(2); //doesn't exist
            ts.MarkComplete(3);
            
            Assert.IsTrue(ts.IsCompleted(1), "Time 1 completed");
            Assert.IsFalse(ts.IsCompleted(2), "Time 2 doesn't exist");
            Assert.IsTrue(ts.IsCompleted(3), "Time 3 completed");
            Assert.IsFalse(ts.IsCompleted(7), "Time 7 is not completed");
            ts.MarkComplete(7);
            Assert.IsTrue(ts.IsCompleted(7), "Time 7 is now completed");
        }
        [TestMethod]
        public void MovementOrderSet_ShouldBeAbleToDetermineIfAnOrderExists_AtASpecificTime()
        {
            var ms = FillMovementOrderSetWithOrders();

            
            Assert.AreEqual(2, ms.Count(1), "time 1 check should return 2 items");
            Assert.AreEqual(0, ms.Count(2), "Time 2 check should return 0 items");

            //Assert.Inconclusive();
        }
        [TestMethod]
        public void MovementOrderSet_ShouldBeAbleToGetAll_TheItems()
        {
            var ms = FillMovementOrderSetWithOrders();

            Assert.AreEqual(15, ms.Count(), "Should have 15 items");
            var list = ms.GetList();

            Assert.AreEqual(15, list.Count, "List should also have 15 items");
        }
        [TestMethod]
        [ExpectedException(typeof(ArgumentOutOfRangeException))]
        public void MovementOrderSet_ShouldThrowException_IfGettingANonExistantItem()
        {
            var ms = FillMovementOrderSetWithOrders();
            ms.Get(2, TypeOfChangeEnum.Course);
            //should scream bloody murder
            Assert.Fail("If you see this then the exception did not get thrown.");
        }
        [Ignore]
        [TestMethod]
        public void MovementOrderSet_ShouldBeAbleToInsertSingleOrder_AtAnyPointInTheList_AndReplaceAllLaterPoints()
        {
            //considerations:  Do I move later items?  How much?  If the insert between time 1 and 10 is a 5
            // do I move 10 to a 15?  Will I ever do this?  I can currently insert an item, and modify an item
            //so this may not be valuable.  TODO: Defer this feature until needed.
            Assert.Inconclusive();
        }
        [TestMethod]
        public void MovementOrderSet_ShouldBeAbleToRemoveAnItem_GivenATime()
        {
            var ms = new MovementOrderSet("Juniper Sloan");
            ms = FillMovementOrderSetWithOrders();
            Assert.AreEqual(15, ms.Count(), "Should be 15");
            ms.Remove(1, TypeOfChangeEnum.Course);

            Assert.AreEqual(14, ms.Count(), "Should be 14");
        }
        [TestMethod]
        public void MovementOrderkSet_ShouldBeAbleToGetTheNextOrders_GivenATime()
        {
            var ms = new MovementOrderSet("Bombadier General");
            ms = FillMovementOrderSetWithOrders();
            /* Movement Order Pattern is:
             *  Time Action  Amount  Expected Value
             *  0    Left    S 270d
             *  0    Down    S 100d
             *  0    Faster  S 8d
             *  1    Right   S 1d    
             *  1    Up      S 5d    
             *  3    Left    S 1d    
             *  4    Right   R 1d    
             *  7    Left    R 1d    
             *  7    Down    S 1d    
             *  8    Down    R 1d    
             *  9    Up      R 1d    
             *  12   Faster  S 5d   
             *  15   Slower  S 10d  
             *  18   Faster  R 30d  
             *  20   Slower  R 5d   
             */
            var courseResult = ms.GetNextOrder(1, TypeOfChangeEnum.Course);
            var speedResult = ms.GetNextOrder(1, TypeOfChangeEnum.Speed);
            var depthResult = ms.GetNextOrder(1, TypeOfChangeEnum.Depth);
            //assert left turn time 3 1 degree            
            Assert.AreEqual(3, courseResult.StartTimeInSeconds,"Time 1 next course time should be 3");
            Assert.AreEqual(1, courseResult.OrderValue, "Time 1 next course value is 1");
            Assert.AreEqual(TypeOfChangeEnum.Course, courseResult.TypeOfChange(), "Time 1 should be a course type");
            Assert.AreEqual(-1, courseResult.DirectionMultiplier, "Time 1 should be a left turn");
            //assert speed increase time 12 5 knots
            Assert.AreEqual(12, speedResult.StartTimeInSeconds, "Time 1 next speed time should be 12");
            Assert.AreEqual(5, speedResult.OrderValue, "Time 1 next speed value is 5");
            Assert.AreEqual(TypeOfChangeEnum.Speed, speedResult.TypeOfChange(), "Time 1 should be a speed type");
            Assert.AreEqual(1, speedResult.DirectionMultiplier, "Time 1 should be an increase in speed");
            //assert depth decrease time 7 1d
            Assert.AreEqual(7, depthResult.StartTimeInSeconds, "Time 1 next depth time should be 7");
            Assert.AreEqual(1, depthResult.OrderValue, "Time 1 next depth value is 1");
            Assert.AreEqual(TypeOfChangeEnum.Depth, depthResult.TypeOfChange(), "Time 1 should be a depth type");
            Assert.AreEqual(1, depthResult.DirectionMultiplier, "Time 1 should be down");
                    
        }
        [TestMethod]
        public void MovementOrderSet_ShouldRetreiveMultipleRunningOrders_GivenATime()
        {
            //TODO: Fix the Fill Methods to start with a valid C/S/D at time 0 and update tests.
            //Rule is return all the current orders that are valid at a given time.  
            //(Generally one of each TypeOfChangeEnum)
            //This will then require a base order for Time 0 for each type.
            MovementOrderSet ms = FillMovementOrderSetWithOrders();
            /* Movement Order Pattern is:
             *  Time Action  Amount  Set Values C/S/D
             *  0    Left    S 270d  270/./.
             *  0    Down    S 100d  270/./100
             *  0    Faster  S 8d    270/8/100
             *  1    Right   S 1d    1/8/100
             *  1    Up      S 5d    1/8/5
             *  3    Left    S 1d    1/8/5
             *  4    Right   R 1d    1/8/5
             *  7    Left    R 1d    1/8/5
             *  7    Down    S 1d    1/8/1
             *  8    Down    R 1d    1/8/1
             *  9    Up      R 1d    1/8/1
             *  12   Faster  S 5d    1/5/1 TEST
             *  15   Slower  S 10d   1/10/1 TEST
             *  18   Faster  R 30d   1/30/1 TEST
             *  20   Slower  R 5d    1/5/1 TEST
             */

            var testOrder12 = ms.GetCurrentOrders(12);  //List of one c/s/d
            var testOrder15 = ms.GetCurrentOrders(15);
            var testOrder18 = ms.GetCurrentOrders(18);
            var testOrder20 = ms.GetCurrentOrders(20);
            Assert.AreEqual(3, testOrder12.Count(), "time 12 Should have returned 3 orders");
            Assert.AreEqual(3, testOrder15.Count(), "time 15 Should have returned 3 orders");
            Assert.AreEqual(3, testOrder18.Count(), "time 18 Should have returned 3 orders");
            Assert.AreEqual(3, testOrder20.Count(), "time 20 Should have returned 3 orders");
            
            Assert.AreEqual(1, getValueFromOrderEnum(TypeOfChangeEnum.Course, testOrder12),
                "Time 12 should return course=1");
            Assert.AreEqual(5, getValueFromOrderEnum(TypeOfChangeEnum.Speed,  testOrder12),
                "Time 12 should return speed=5");
            Assert.AreEqual(1, getValueFromOrderEnum(TypeOfChangeEnum.Depth,  testOrder12),
                "Time 12 should return depth=1");

            Assert.AreEqual(1, getValueFromOrderEnum(TypeOfChangeEnum.Course, testOrder15),
                "Time 15 should return course=1");
            Assert.AreEqual(10, getValueFromOrderEnum(TypeOfChangeEnum.Speed,  testOrder15),
                "Time 15 should return speed=10");
            Assert.AreEqual(1, getValueFromOrderEnum(TypeOfChangeEnum.Depth,  testOrder15),
                "Time 15 should return depth=1");
            
            Assert.AreEqual(1, getValueFromOrderEnum(TypeOfChangeEnum.Course, testOrder18),
                "Time 18 should return course=1");
            Assert.AreEqual(30, getValueFromOrderEnum(TypeOfChangeEnum.Speed,  testOrder18),
                "Time 18 should return speed=30");
            Assert.AreEqual(1, getValueFromOrderEnum(TypeOfChangeEnum.Depth,  testOrder18),
                "Time 18 should return depth=1");
            
            Assert.AreEqual(1, getValueFromOrderEnum(TypeOfChangeEnum.Course, testOrder20),
                "Time 20 should return course=1");
            Assert.AreEqual(5, getValueFromOrderEnum(TypeOfChangeEnum.Speed,  testOrder20),
                "Time 20 should return speed=5"); 
            Assert.AreEqual(1, getValueFromOrderEnum(TypeOfChangeEnum.Depth,  testOrder20),
                "Time 20 should return depth=1");


        }


        [TestMethod]
        public void MovemementOrderSet_ShouldRetreiveSingleRunningOrder_GivenATimeAndType()
        {
            MovementOrderSet ms = FillMovementOrderSetWithCourseChanges();
            /* Movement Order Pattern is:
              * Time Action Amount
              *  0    Left    S 270d
              *  0    Down    S 100d
              *  0    Faster  S 8d              
              *  1   Right   5d  S
              *  5   Left    10d R
              *  10  Left    15d R
              *  15  Right   20d R
              */
            //  Time *=doesn't exist 0*, 1  2  3  4   5   6   7   8   9  10  11  12  13  14  15 
            int[] expectedValues =  {270 , 5, 5, 5, 5, 10, 10, 10, 10, 10, 15, 15, 15, 15, 15, 15};

            var firstStartTime = ms.GetList().First(o => o.TypeOfChange() == TypeOfChangeEnum.Course).StartTimeInSeconds;
            var lastStartTime  = ms.GetList().Last (o => o.TypeOfChange() == TypeOfChangeEnum.Course).StartTimeInSeconds;
            for (var timeSegment = firstStartTime ; timeSegment != lastStartTime; timeSegment++)
            {
                var result =  ms.GetCurrentOrder(timeSegment, TypeOfChangeEnum.Course).OrderValue;
                Assert.AreEqual(expectedValues[timeSegment], result,
                    String.Format("Unexpected Order at time {0}, expected {1}, received {2}",
                        timeSegment, expectedValues[timeSegment], result));
            }
        
        }
        
        [TestMethod]
        public void MovementOrderSet_ShouldBeAbleToInsertAnotherSetAtAnyPointInTheSet_AndReplaceAllLaterPoints()
        {
            //var currentMovement = new Movement(0d, 0d, 0d);
            var ms = FillMovementOrderSetWithOrders( );
            var ms2 = FillInsertionMovementOrderSet();
            const int pointInSet = 3;
            ms.InsertSetReplaceEnd(ms2, pointInSet);  
            
            //assert  post = time 1, 3, 4,  8, 12 
            //              value 1, 5, 5, 15, 10
            Assert.AreEqual(1,  ms.Get(1,  TypeOfChangeEnum.Course).OrderValue, "time 1  = course set to 1");
            Assert.AreEqual(5,  ms.Get(3,  TypeOfChangeEnum.Course).OrderValue, "time 3  = course set to 5");
            Assert.AreEqual(5,  ms.Get(4,  TypeOfChangeEnum.Course).OrderValue, "time 4  = course set to 5");
            Assert.AreEqual(15, ms.Get(8,  TypeOfChangeEnum.Course).OrderValue, "time 8  = course set to 15");
            Assert.AreEqual(10, ms.Get(12, TypeOfChangeEnum.Course).OrderValue, "time 12 = course set to 10");
        }
        [TestMethod]
        public void MovementOrderSet_ShouldInsertSetInMiddle()
        {
            var ms = FillMovementOrderSetWithOrders();
            var ms2 = FillInsertionMovementOrderSet();
 
            ms.InsertSet(ms2, 3);
            Assert.AreEqual(19, ms.Count(), "Should have 19 orders now");
            /* Post insert should be:
             * Time OT   Action  Amount
             *  0    Left    S 270d
             *  0    Down    S 100d
             *  0    Faster  S 8d
             *  1   1    Right   S 1d    1d C
             *  1   1    Up      S 5d    5d D - duplicate time
             ** 6   3    Right   5d
             ** 7   4    Left    5d
             ** 11  8    Left    15d
             ** 15  12   Left    10d
             *  19  3    Left    S 1d    1d C
             *  20  4    Right   R 1d    1d C
             *  23  7    Left    R 1d    1d C
             *  23  7    Down    S 1d    1d D
             *  24  8    Down    R 1d    2d D
             *  25  9    Up      R 1d    1d D
             *  28  12   Faster  S 5d    5d S
             *  31  15   Slower  S 10d   10d S
             *  34  18   Faster  R 30d   30d S - Limit throttle to 30
             *  36  20   Slower  R 5d    25d S
           
             */
            int[] expectedTimes =     {  0,  0,  0, 1, 1, 6, 7, 11, 15, 19, 20, 23, 23, 24, 25, 28, 31, 34, 36 };
            double[] expectedValues = {270, 100, 8, 1, 5, 5, 5, 15, 10,  1,  1,  1,  1,  1,  1, 5,  10, 30, 5  };
            var theList = ms.GetList();
            for (var i = 0; i < theList.Count; i++)
            {
                Assert.AreEqual(expectedTimes[i], theList[i].StartTimeInSeconds, string.Format("The item at expected time {0} had a time of {1}", expectedTimes[i], theList[i].StartTimeInSeconds));
                Assert.AreEqual(expectedValues[i], theList[i].OrderValue, string.Format("The order at time {0} expected Value {1} but had a value of {2}", expectedTimes[i], expectedValues[i], theList[i].OrderValue));
            }


        }
        [TestMethod]
        public void MovementOrderSet_ShouldInsertSetAtEnd()
        {
            var ms = FillMovementOrderSetWithOrders();
            var ms2 = FillInsertionMovementOrderSet();

            ms.InsertSetAtEnd(ms2);
            Assert.AreEqual(19, ms.Count(), "Should have 19 orders now");
            /* Test Order Pattern is:
             *  Time Action  Amount  Expected Value
             *  0    Left    S 270d
             *  0    Down    S 100d
             *  0    Faster  S 8d
             *  1     Right   S 1d    
             *  1     Up      S 5d    
             *  3     Left    S 1d    
             *  4     Right   R 1d    
             *  7     Left    R 1d    
             *  7     Down    S 1d    
             *  8     Down    R 1d    
             *  9     Up      R 1d    
             *  12    Faster  S 5d   
             *  15    Slower  S 10d  
             *  18    Faster  R 30d  
             *  20    Slower  R 5d               
             ** 24 3   Right  S 5d
             ** 25 4   Left   R 5d
             ** 29 8   Left     15d
             ** 33 12  Left     10d
             */
            int[] expectedTimes =     {   0,   0, 0, 1, 1, 3, 4, 7, 7, 8, 9, 12, 15, 18, 20, 24, 25, 29, 33 };
            double[] expectedValues = { 270, 100, 8, 1, 5, 1, 1, 1, 1, 1, 1,  5, 10, 30,  5,  5,  5, 15, 10 };
            var theList = ms.GetList();
            for (var i = 0; i < theList.Count; i++)
            {
                Assert.AreEqual(expectedTimes[i], theList[i].StartTimeInSeconds, string.Format("The item at expected time {0}, Received time {1}", expectedTimes[i], theList[i].StartTimeInSeconds));
                Assert.AreEqual(expectedValues[i], theList[i].OrderValue, string.Format("The order at {0} expected Value {1}, Received Value {2}", expectedTimes[i], expectedValues[i], theList[i].OrderValue));
            }
        }
        [TestMethod]
        public void MovementOrderSet_ShouldInsertSetAtBeginning()
        {
            var ms = FillMovementOrderSetWithOrders();
            var ms2 = FillInsertionMovementOrderSet();

            ms.InsertSet(ms2);
            Assert.AreEqual(19, ms.Count(), "Should have 19 orders now");
            /* Test Order Pattern is:
            *  Time Action  Amount  Expected Value

            **  3       Right   S 5d
            **  4       Left    R 5d
            **  8       Left    R 15d
            **  12      Left    R 10d
            *  13 0       Left    S 270d
            *  13 0       Down    S 100d
            *  13 0       Faster  S 8d            
            *  14 1    Right   S 1d   
            *  14 1    Up      S 5d   
            *  16 3    Left    S 1d   
            *  17 4    Right   R 1d   
            *  20 7    Left    R 1d   
            *  20 7    Down    S 1d   
            *  21 8    Down    R 1d   
            *  22 9    Up      R 1d   
            *  25 12   Faster  S 5d   
            *  28 15   Slower  S 10d   
            *  31 18   Faster  R 30d   
            *  33 20   Slower  R 5d    
            */
            
            int[] expectedTimes =     {  3,  4, 8,  12,  13,  13, 13, 14, 14, 16, 17, 20, 20, 21, 22, 25, 28, 31, 33 };
            double[] expectedValues = {  5,  5, 15, 10, 270, 100,  8,  1,  5,  1,  1,  1,  1,  1,  1,  5, 10, 30,  5 };
            var theList = ms.GetList();
            for (var i = 0; i < theList.Count; i++)
            {
                Assert.AreEqual(expectedTimes[i], theList[i].StartTimeInSeconds, string.Format("The item at expected time {0}, Received time {1}", expectedTimes[i], theList[i].StartTimeInSeconds));
                Assert.AreEqual(expectedValues[i], theList[i].OrderValue, string.Format("The order at {0} expected Value {1}, Received Value {2}", expectedTimes[i], expectedValues[i], theList[i].OrderValue));
            }


        }
        [TestMethod]
        public void MovementOrderSet_ShouldBeAbleToChangeAnExistingItem_Given_NewOrder()
        {
            var ms = FillMovementOrderSetWithOrders();
            var changedOrder = new StaticCourseChangeRightOrder(3, 90d, FillMovementLimits());
            
            Assert.IsTrue(ms.Exists(3, TypeOfChangeEnum.Course), 
                "Orders has an entry at time 3");
            ms.Replace(changedOrder);
            Assert.AreEqual(90, ms.Get(3, TypeOfChangeEnum.Course).OrderValue, 
                "OrderedCourse at time 3 should now be 90");
           
        }
    
        private static MovementOrderSet FillInsertionMovementOrderSet()
        {
            var limits = FillMovementLimits();
            
            var ms2 = new MovementOrderSet("Number two");
            //AddInitialCSD(limits, ms2);
            var turnRightTo5Degrees =   new StaticCourseChangeRightOrder ( 3,  5d,  limits);
            var turnLeft5Degrees =      new RelativeCourseChangeLeftOrder( 4,  5d,  limits);
            var turnLeft15Degrees =     new RelativeCourseChangeLeftOrder( 8,  15d, limits);
            var turnLeft10Degrees =     new RelativeCourseChangeLeftOrder( 12, 10d, limits);
            /* Movement Order Pattern is:
             * Time Action Amount
             *  3   Right   5d
             *  4   Left    5d
             *  8   Left    15d
             *  12  Left    10d
             */


            ms2.Add(turnRightTo5Degrees);
            ms2.Add(turnLeft5Degrees);
            ms2.Add(turnLeft15Degrees);
            ms2.Add(turnLeft10Degrees);
            return ms2;
        }
        private static MovementOrderSet FillMovementOrderSetWithCourseChanges()
        {
            var limits = FillMovementLimits();

            var ms2 = new MovementOrderSet("Number two");
            AddInitialCsd(limits, ms2);
            var turnRightTo5Degrees = new StaticCourseChangeRightOrder(1, 5d, limits);
            var turnLeft10Degrees = new RelativeCourseChangeLeftOrder(5, 10d, limits);
            var turnLeft15Degrees = new RelativeCourseChangeLeftOrder(10, 15d, limits);
            var turnRight20Degrees = new RelativeCourseChangeRightOrder(15, 20d, limits);
            /* Movement Order Pattern is:
             * Time Action Amount
             *  0    Left    S 270d
             *  0    Down    S 100d
             *  0    Faster  S 8d
             *  1   Right   5d  S
             *  5   Left    10d R
             *  10  Left    15d R
             *  15  Right   20d R
             */


            ms2.Add(turnRightTo5Degrees);
            ms2.Add(turnLeft10Degrees);
            ms2.Add(turnLeft15Degrees);
            ms2.Add(turnRight20Degrees);
            return ms2;
        }
        private static MovementOrderSet FillMovementOrderSetWithOrders()
        {
            var limits = FillMovementLimits();
            var ms = new MovementOrderSet("MainTestSet");
            /* Total Orders:  15 total
             * 
             /* Movement Order Pattern is:
             *  Time Action  Amount  Expected Value
             *  0    Left    S 270d
             *  0    Down    S 100d
             *  0    Faster  S 8d
             *  1    Right   S 1d    
             *  1    Up      S 5d    
             *  3    Left    S 1d    
             *  4    Right   R 1d    
             *  7    Left    R 1d    
             *  7    Down    S 1d    
             *  8    Down    R 1d    
             *  9    Up      R 1d    
             *  12   Faster  S 5d   
             *  15   Slower  S 10d  
             *  18   Faster  R 30d  
             *  20   Slower  R 5d   
             */
            AddInitialCsd(limits, ms);
            var orderStaticRight =    new StaticCourseChangeRightOrder    ( 1,  1d,  limits);
            var orderStaticUp =       new StaticDepthChangeUpOrder        ( 1,  5d,  limits);
            var orderStaticLeft =     new StaticCourseChangeLeftOrder     ( 3,  1d,  limits);
            var orderRelativeRight =  new RelativeCourseChangeRightOrder  ( 4,  1d,  limits);
            var orderRelativeLeft =   new RelativeCourseChangeLeftOrder   ( 7,  1d,  limits);
            var orderStaticDown =     new StaticDepthChangeDownOrder      ( 7,  1d,  limits);
            var orderRelativeDown =   new RelativeDepthChangeDownOrder    ( 8,  1d,  limits);
            var orderRelativeUp =     new RelativeDepthChangeUpOrder      ( 9,  1d,  limits);
            var orderStaticFaster=    new StaticSpeedChangeIncreaseOrder  ( 12, 5d,  limits);
            var orderStaticSlower =   new StaticSpeedChangeDecreaseOrder  ( 15, 10d, limits);
            var orderRelativeFaster = new RelativeSpeedChangeIncreaseOrder( 18, 30d, limits);
            var orderRelativeSlower = new RelativeSpeedChangeDecreaseOrder( 20, 5d,  limits);

            ms.Add(orderStaticRight);
            ms.Add(orderStaticLeft); 
            ms.Add(orderRelativeRight);
            ms.Add(orderRelativeLeft); 
            ms.Add(orderStaticDown); //duplicate time 7 
            ms.Add(orderStaticUp); //duplicate time 1
            ms.Add(orderRelativeDown);
            ms.Add(orderRelativeUp);
            ms.Add(orderStaticFaster);
            ms.Add(orderStaticSlower);
            ms.Add(orderRelativeFaster); //exceeds limit
            ms.Add(orderRelativeSlower);
            return ms;
        }

        private static void AddInitialCsd(MovementCharacteristics limits, MovementOrderSet ms)
        {
            /* Movement Order Pattern is:
             *  Time Action  Amount  Expected Value
             *  0    Left    S 270d
             *  0    Down    S 100d
             *  0    Faster  S 8d
             */
            var orderInitCourse = new StaticCourseChangeLeftOrder(0, 270d, limits);
            var orderInitSpeed = new StaticSpeedChangeIncreaseOrder(0, 8d, limits);
            var orderInitDepth = new StaticDepthChangeDownOrder(0, 100d, limits);
            ms.Add(orderInitCourse);
            ms.Add(orderInitDepth);
            ms.Add(orderInitSpeed);

        }

        private static MovementCharacteristics FillMovementLimits()
        {
            var limits = new MovementCharacteristics 
                 {MinDepth = -1000, MaxDepth = 1000, MinSpeed = 0, MaxSpeed = 30};
            return limits;
        }
        private double getValueFromOrderEnum(TypeOfChangeEnum theEnum, IEnumerable<MovementOrderBase> theList)
        {
            return theList.First(o => o.TypeOfChange() == theEnum).OrderValue;
        }
    }
    
}
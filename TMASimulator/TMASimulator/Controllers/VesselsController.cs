﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TMASimulator.Models;

namespace TMASimulator.Controllers
{
    public class VesselsController : Controller
    {
        // GET: Vessels
        public ActionResult Index()
        {
            VesselSet vs = new VesselSet();
            vs.AddVessel(new Vessel(1, new VesselType(1, "VT1")));
            vs.AddVessel(new Vessel(2, new VesselType(1, "VT1")));
            vs.AddVessel(new Vessel(3, new VesselType(1, "VT1")));
            vs.AddVessel(new Vessel(4, new VesselType(2, "VT2")));
            vs.AddVessel(new Vessel(5, new VesselType(2, "VT2")));

            ViewData["Vessels"] = vs;
            return View("View");
        }

    }
}
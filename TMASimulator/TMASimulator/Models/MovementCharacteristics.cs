﻿namespace TMASimulator.Models
{
    public class MovementCharacteristics
    {
        private bool _hasBeenSet;
        private double _minSpeed;
        private double _maxSpeed;
        private double _minDepth;
        private double _maxDepth;
        private double _speedChangeRate;
        private double _depthChangeRate;
        private double _courseChangeRate;

        public double MinSpeed
        {
            get { return _minSpeed; }
            set
            {
                _minSpeed = value;
                _hasBeenSet = true;
            }
        }

        public double MaxSpeed
        {
            get { return _maxSpeed; }
            set
            {
                _maxSpeed = value;
                _hasBeenSet = true;
            }
        }

        public double MinDepth  
        {
            get { return _minDepth; }
            set
            {
                _minDepth = value;
                _hasBeenSet = true;
            }
        }

        public double MaxDepth
        {
            get { return _maxDepth; }
            set
            {
                _maxDepth = value;
                _hasBeenSet = true;
            }
        }

        public double SpeedChangeRate
        {
            get { return _speedChangeRate; }
            set
            {
                _speedChangeRate = value;
                _hasBeenSet = true;
            }
        }

        public double DepthChangeRate
        {
            get { return _depthChangeRate; }
            set
            {
                _depthChangeRate = value;
                _hasBeenSet = true;
            }
        }

        public double CourseChangeRate
        {
            get { return _courseChangeRate; }
            set
            {
                _courseChangeRate = value;
                _hasBeenSet = true;
            }
        }

        public bool HasBeenSet
        {
            get { return _hasBeenSet; }
        }
    }
}
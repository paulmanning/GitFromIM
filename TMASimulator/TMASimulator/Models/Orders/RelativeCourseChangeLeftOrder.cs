namespace TMASimulator.Models.Orders
{
    public class RelativeCourseChangeLeftOrder: MovementOrderBase
    {
        public RelativeCourseChangeLeftOrder(int startTime, double orderValue, MovementCharacteristics limits) : base(startTime, orderValue, limits)
        {
            DirectionMultiplier = -1;
          //  base.OrderValue = CleanCourse((360- orderValue) + currentValue);
        }

        public override TypeOfChangeEnum TypeOfChange()
        {
            return TypeOfChangeEnum.Course;
        }

        public override RelativeStaticEnum TypeOfChangeRelativeStatic()
        {
            return RelativeStaticEnum.Relative;
        }

        //todo: remove currentMovement property
        public override MovementOrderBase GetNewOfMeFromCurrentOrder()
        {
            return new RelativeCourseChangeLeftOrder( StartTimeInSeconds, OrderValue, Limits);
        }
    }
}
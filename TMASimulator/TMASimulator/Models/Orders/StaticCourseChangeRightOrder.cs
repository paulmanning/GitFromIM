namespace TMASimulator.Models.Orders
{
    public class StaticCourseChangeRightOrder: MovementOrderBase
    {
        public StaticCourseChangeRightOrder(int startTime, double orderValue, MovementCharacteristics limits) : base(startTime, orderValue, limits)
        {
            DirectionMultiplier = 1;
            OrderValue = CleanCourse(OrderValue);
        }

        public override TypeOfChangeEnum TypeOfChange()
        {
            return TypeOfChangeEnum.Course;
        }

        public override RelativeStaticEnum TypeOfChangeRelativeStatic()
        {
            return RelativeStaticEnum.Static;
        }

        public override MovementOrderBase GetNewOfMeFromCurrentOrder()
        {
            return new StaticCourseChangeRightOrder( StartTimeInSeconds, OrderValue, Limits);
        }
    }
}
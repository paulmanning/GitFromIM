namespace TMASimulator.Models.Orders
{
    public class StaticSpeedChangeDecreaseOrder:MovementOrderBase
    {
        public StaticSpeedChangeDecreaseOrder(int startTime, double orderValue, MovementCharacteristics limits) : base(startTime, orderValue, limits)
        {
            DirectionMultiplier = -1;
            ApplyLimits(Limits.MinSpeed, Limits.MaxSpeed);

        }
        public override TypeOfChangeEnum TypeOfChange()
        {
            return TypeOfChangeEnum.Speed;
        }

        public override RelativeStaticEnum TypeOfChangeRelativeStatic()
        {
            return RelativeStaticEnum.Static;
        }

        public override MovementOrderBase GetNewOfMeFromCurrentOrder()
        {
            return new StaticSpeedChangeDecreaseOrder( StartTimeInSeconds, OrderValue, Limits);
        }
    }
}
namespace TMASimulator.Models.Orders
{
    public class RelativeCourseChangeRightOrder : MovementOrderBase
    {
        public RelativeCourseChangeRightOrder(int startTime, double orderValue, MovementCharacteristics limits) : base(startTime, orderValue, limits)
        {
       DirectionMultiplier = 1;
       
        }

        public override TypeOfChangeEnum TypeOfChange()
        {
            return TypeOfChangeEnum.Course;
        }

        public override RelativeStaticEnum TypeOfChangeRelativeStatic()
        {
            return RelativeStaticEnum.Relative;
        }

        public override MovementOrderBase GetNewOfMeFromCurrentOrder()
        {
            return new RelativeCourseChangeRightOrder(StartTimeInSeconds, OrderValue, Limits);
        }
    }
}
﻿using System;
using TMASimulator.Models.Orders;

namespace TMASimulator.Models
{
    public class Movement
    {
        public const int Left = -1;
        public const int Right = 1;
        private double _course;
        private const double NmToMeter = 1852;
        private readonly bool _isChanging;
        //private const double MeterToNm = 0.00053996;

        public Movement NextMovement(TimeSpan t, MovementCharacteristics mc, MovementOrderBase order)
        {
            var nextCourse = Course;
            var nextSpeed = Speed;
            var nextDepth = Depth;
            var isChangingCourseAfterOrder = false;
            var isChangingDepthAfterOrder =false;
            var isChangingSpeedAfterOrder = false;

            
            if (order.TypeOfChange()== TypeOfChangeEnum.Course)
            {
                var degreesRemainingtoTurn = GetDirectionCorrectedDegreesRemainingToTurn(order);
                var degreesToTurn = mc.CourseChangeRate*t.TotalSeconds;
                isChangingCourseAfterOrder = IsStillChanging(degreesToTurn,  degreesRemainingtoTurn);
                nextCourse= Course + (order.DirectionMultiplier*AmountToTurn(degreesToTurn, degreesRemainingtoTurn));
            }
            if (order.TypeOfChange()==TypeOfChangeEnum.Depth)
            {
                var metersRemainingToChange = Math.Abs(Depth - order.OrderValue);
                var amountToChange = mc.DepthChangeRate * t.TotalSeconds;
                amountToChange = (amountToChange >= metersRemainingToChange) 
                    ? metersRemainingToChange 
                    : amountToChange; 
                isChangingDepthAfterOrder = IsStillChanging(amountToChange, metersRemainingToChange);
                nextDepth = Depth + amountToChange * (order.OrderValue > Depth ? 1 : -1);
            }
            if (order.TypeOfChange()==TypeOfChangeEnum.Speed)
            {
                var knotsRemainingToChange = Math.Abs(Speed - order.OrderValue);
                var knotsToChange = mc.SpeedChangeRate*t.TotalSeconds;
                knotsToChange = (knotsToChange >= knotsRemainingToChange)
                    ? knotsRemainingToChange 
                    : knotsToChange; 
                isChangingSpeedAfterOrder = IsStillChanging(knotsToChange, knotsRemainingToChange);
                nextSpeed = Speed + knotsToChange * (order.OrderValue > Speed ? 1 : -1);
            }

            var isChangingAfterOrder = isChangingCourseAfterOrder || isChangingDepthAfterOrder || isChangingSpeedAfterOrder;
            return new Movement(nextCourse, nextSpeed, nextDepth, isChangingAfterOrder);            

        }

        private double GetDirectionCorrectedDegreesRemainingToTurn(MovementOrderBase order)
        {
            //todo static/relative
            return (order.DirectionMultiplier == -1)
                ? LeftCorrectedCourseDegrees(order.OrderValue)
                : RightCorrectedCourseDegrees(order.OrderValue);
           
        }

        private double RightCorrectedCourseDegrees(double orderValue)
        {
            return Course > orderValue
                ? (360 + Course) - orderValue
                : orderValue - Course;
        }

        private double LeftCorrectedCourseDegrees(double orderValue)
        {
            return Course < orderValue
                ? (360 + Course) - orderValue
                : orderValue - Course;
        }

        private bool IsStillChanging (double itemToChange, double remainingToChange)
        {
            return !(itemToChange >= remainingToChange);
        }
        private double AmountToTurn (double degreesToTurn, double degreesRemainingToTurn)
        {
           return degreesToTurn >= degreesRemainingToTurn? degreesRemainingToTurn: degreesToTurn;    
        }
         
        public Movement(double course, double speed, double depth)
        {
            Course = course;
            Speed = speed;
            Depth = depth;
            _isChanging = false;
        }

        private Movement(double course, double speed, double depth, bool isChanging)
        {
            Course = course;
            Speed = speed;
            Depth = depth;
            _isChanging = isChanging;
        }

        public double GetDistanceTraveledNm(TimeSpan t)
        {
            return t.TotalHours*Speed;
        }

        public double GetDistanceTraveledMeters(TimeSpan t)
        {
            return (t.TotalHours*Speed)*NmToMeter;
        }
        public double Course
        {
            get { return _course; }
            set { _course = CleanCourse(value); }
        }

        private double CleanCourse(double value)
        {
            if (value >= 360)
            {
                return value%360;
            }
            if (value < 0)
            {
                return 360 - Math.Abs(value)%360;
            }
            return value;
        }

        public double GetByType(TypeOfChangeEnum theChangeType)
        {
            Double result;
            switch (theChangeType)
            {
                case TypeOfChangeEnum.Course:
                    result = Course;
                    break;
                case TypeOfChangeEnum.Speed:
                    result = Speed;
                    break;
                case TypeOfChangeEnum.Depth:
                    result = Depth;
                    break;
                default:
                    throw new ArgumentOutOfRangeException("theChangeType");
            }
            return result;
        }

        public void SetByType(double value, TypeOfChangeEnum theChangeType)
        {
            switch (theChangeType)
            {
                case TypeOfChangeEnum.Course:
                    Course = value;
                    break;
                case TypeOfChangeEnum.Speed:
                    Speed = value;
                    break;
                case TypeOfChangeEnum.Depth:
                    Depth = value;
                    break;
                default:
                    throw new ArgumentOutOfRangeException("theChangeType");
            }
        }
        public double Speed { get; set; }
        public double Depth { get; set; }

        public bool IsChanging
        {
            get { return _isChanging; }
        }
    }
}